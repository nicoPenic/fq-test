# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Complete the exercise number 5 of the technical test

### How do I get set up? ###

* git clone git@bitbucket.org:nicoPenic/fq-test.git
* cp .env.example .env && mkdir storage/framework storage/framework/sessions storage/framework/views storage/framework/cache
* composer install
* php artisan key:generate

### How do I run the code? ###

* php artisan serve
* go to http://127.0.0.1:8000
