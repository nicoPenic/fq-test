<?php

namespace App\Http\Controllers;

use App\Library\Constants;
use Illuminate\Http\Request;

/**
 * Generic class to simulate a simple version of a shopping cart, simulating exercise 5
 *
 * Class GlobalController
 * @package App\Http\Controllers
 */
class GlobalController extends Controller
{
    /**
     * @var array
     */
    private $vouchersAdded;
    /**
     * @var int
     */
    private $total = 0;

    /**
     * Serves the basic view for shopping cart adding simulation
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        // Get generic data for products and vouchers
        $products = config('generic-items.products');
        $vouchers = config('generic-items.vouchers');

        return view('addProducts', compact('products', 'vouchers'));
    }

    /**
     * Processes the products and vouchers added to the shopping cart
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function shoppingCart(Request $request) {

        // Get products and vouchers sent by user
        $productsAdded          = $request->get('products');
        $this->vouchersAdded    = $request->get('vouchers');

        // Process product depending on quantity
        foreach ($productsAdded as $product => $quantity) {
            if ($quantity) {
                $this->processProduct($product, $quantity);
            }
        }

        // There can be some vouchers unused and not linked to any product that must be applied to the whole cart (global)
        if (!empty($this->vouchersAdded)) {
            foreach ($this->vouchersAdded as $globalVoucherName => $value) {
                $this->processGlobalVoucher($globalVoucherName);
            }
        }

        // Get generic data for products and vouchers
        $products   = config('generic-items.products');
        $vouchers   = config('generic-items.vouchers');
        $total      = $this->total;

        return view('addProducts', compact('products', 'vouchers', 'total'));
    }

    /**
     * Processes vouchers that must be applied to the whole cart
     *
     * @param $voucherName
     */
    private function processGlobalVoucher($voucherName)
    {
        // Get the voucher data from generic items
        $voucher = $this->getVoucher($voucherName);

        if ($voucher['apply_on'] == Constants::DISCOUNT_CONDITION_WHOLE_CART && !empty($voucher['conditions'])) {
            switch ($voucher['conditions']['type']) {
                // Analize predefined conditions
                case Constants::DISCOUNT_CONDITION_GLOBAL_TOTAL_PRICE_BIGGER_THAN:
                    if ($this->total > $voucher['conditions']['value']) {
                        switch ($voucher['type']) {
                            case Constants::DISCOUNT_TYPE_PERCENTAGE:
                                $this->total = round($this->total * (1 - $voucher['amount_decimal']), 2);
                                break;
                            case Constants::DISCOUNT_TYPE_ABSOLUTE:
                                $this->total = round($this->total - $voucher['amount'], 2);
                                break;
                        }
                    }
                    // Delete used vouchers?
                    unset($this->vouchersAdded[$voucher['name']]);
                    break;
                // No more global conditions defined
                default:
                    break;
            }
        }
    }

    /**
     * Processes products and related vouchers
     *
     * @param $productName
     * @param $quantity
     */
    private function processProduct($productName, $quantity)
    {
        // Get the product data from generic items
        $product = $this->getProduct($productName);

        if (!empty($product['voucher'])) {
            if($product['voucher']['conditions']) {
                switch ($product['voucher']['conditions']['type']) {
                    // Analize predefined conditions
                    case Constants::DISCOUNT_CONDITION_SPECIFIC_APPLY_ON_SECOND_UNIT:
                        // Check if condition is satisfied
                        if ($quantity >= $product['voucher']['conditions']['value']) {
                            switch ($product['voucher']['type']) {
                                case Constants::DISCOUNT_TYPE_PERCENTAGE:
                                    $this->total +=
                                        round($product['price'] * (1 - $product['voucher']['amount_decimal']), 2) +
                                        round(((integer)$quantity - 1) * $product['price'], 2);
                                    break;
                                case Constants::DISCOUNT_TYPE_ABSOLUTE:
                                    $this->total += (integer)$quantity * $product['price'] - $product['voucher']['amount'];
                                    break;
                                default:
                                    $this->total += $product['price'];
                                    break;
                            }
                        } else {
                            $this->total += $product['price'];
                        }
                        // Delete used vouchers?
                        unset($this->vouchersAdded[$product['voucher']['name']]);
                        break;
                    // No more specifics conditions defined
                    default:
                        break;
                }
            } else {
                switch ($product['voucher']['type']) {
                    case Constants::DISCOUNT_TYPE_PERCENTAGE:
                        $this->total +=
                            round($product['price'] * (1 - $product['voucher']['amount_decimal']), 2);
                        unset($this->vouchersAdded[$product['voucher']['name']]);
                        break;
                    case Constants::DISCOUNT_TYPE_ABSOLUTE:
                        $this->total += (integer)$quantity * $product['price'] - $product['voucher']['amount'];
                        unset($this->vouchersAdded[$product['voucher']['name']]);
                        break;
                }
            }
        } else {
            $this->total += round($product['price'] * $quantity, 2);
        }
    }

    /**
     * Gets product data from generic items
     *
     * @param $productName
     * @return mixed
     */
    private function getProduct($productName)
    {
        // Get generic data for products and vouchers
        $products = collect(config('generic-items.products'));
        $vouchers = collect(config('generic-items.vouchers'));

        // Get the requested product
        $product = $products->where('name', $productName)->first();

        if ($product) {
            // Get the related voucher
            $voucher = $vouchers->where('apply_on', $product['name'])->first();

            // Only assign voucher as related if the voucher was added by the user
            if (!empty($voucher) && !empty($this->vouchersAdded) && array_key_exists($voucher['name'], $this->vouchersAdded)) {
                $product['voucher'] = $vouchers->where('apply_on', $product['name'])->first();
            }
        }

        return $product;
    }

    /**
     * Gets voucher data from generic items
     *
     * @param $voucherName
     * @return mixed
     */
    private function getVoucher($voucherName)
    {
        // Get generic data for vouchers
        $vouchers = collect(config('generic-items.vouchers'));

        return $vouchers->where('name', $voucherName)->first();
    }
}
