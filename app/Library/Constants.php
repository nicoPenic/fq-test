<?php

namespace App\Library;

/**
 * Class Constants
 * @package App\Library
 */
class Constants
{
    // -------------------------------------------
    //                 DISCOUNTS
    // -------------------------------------------
    const DISCOUNT_CONDITION_SPECIFIC_APPLY_ON_SECOND_UNIT  = 'APPLY_ON_SECOND_UNIT';
    const DISCOUNT_CONDITION_GLOBAL_TOTAL_PRICE_BIGGER_THAN = 'TOTAL_PRICE';
    const DISCOUNT_CONDITION_WHOLE_CART                     = 'CART';
    const DISCOUNT_TYPE_PERCENTAGE                          = 'PERCENTAGE';
    const DISCOUNT_TYPE_ABSOLUTE                            = 'ABSOLUTE';

}
