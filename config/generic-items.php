<?php

use App\Library\Constants;

return [
    'products' => [
        [
            'name'          => 'ProductA',
            'list_name'     => 'Product A',
            'price'         => '10',
        ], [
            'name'          => 'ProductB',
            'list_name'     => 'Product B',
            'price'         => '8',
        ], [
            'name'          => 'ProductC',
            'list_name'     => 'Product C',
            'price'         => '12',
        ],

    ],

    'vouchers' => [
        [
            'name'          => 'VoucherV',
            'list_name'     => 'Voucher V',
            'type'          =>  Constants::DISCOUNT_TYPE_PERCENTAGE,
            'amount'        => '10',
            'amount_decimal'=> 0.10,
            'apply_on'      => 'ProductA',
            'conditions'    => [
                'type'      => Constants::DISCOUNT_CONDITION_SPECIFIC_APPLY_ON_SECOND_UNIT,
                'value'     => 2
            ]
        ], [
            'name'          => 'VoucherR',
            'list_name'     => 'Voucher R',
            'type'          => Constants::DISCOUNT_TYPE_ABSOLUTE,
            'amount'        => 5,
            'apply_on'      => 'ProductB',
            'conditions'    => false
        ], [
            'name'          => 'VoucherS',
            'type'          =>  Constants::DISCOUNT_TYPE_PERCENTAGE,
            'list_name'     => 'Voucher S',
            'amount'        => '5',
            'amount_decimal'=> 0.05,
            'apply_on'      => Constants::DISCOUNT_CONDITION_WHOLE_CART,
            'conditions'    => [
                'type'      => Constants::DISCOUNT_CONDITION_GLOBAL_TOTAL_PRICE_BIGGER_THAN,
                'value'     => 40
            ]
        ],

    ],
];
