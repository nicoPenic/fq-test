
$(window).on('load', function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    alert('aaaaa');

    $('.request-actions').on('click', function () {
        var requestForm = $('#match-request-form');
        var requestAction = $('<input>')
            .attr('type', 'hidden')
            .attr('name', 'action').val($(this).attr('id'));
        requestForm.append(requestAction);
        requestForm.submit();
    });

    $('.match-actions').on('click', function () {
        var formUpdateStatus = $('#updateStatus');

        var inputReason = $('<input>')
            .attr('type', 'hidden')
            .attr('name', 'reason').val($('#discard-options').val());

        var inputReasonTxt = $('<input>')
            .attr('type', 'hidden')
            .attr('name', 'reason-text').val($('#text-reason').val());

        $('#action').val($(this).data('action'));
        formUpdateStatus.append(inputReason).append(inputReasonTxt);
        formUpdateStatus.submit();
    });

    $('#contact-request').on('click', function () {

        var isSubscribed = $(this).data('subscribed');
        var isSuggestion = $('#is-suggestion').val();

        $.ajax({
            type: 'POST',
            url: '/ajax/send-contact-request',
            data: {
                matchId: $(this).data('matchid'),
                isSubscribed: isSubscribed,
                isSuggestion: isSuggestion
            },
            error: function (jqXHR) {
                var resp = JSON.parse(jqXHR.responseText);

                $('#empty-alert-error > .alert__body > .empty').text(resp.message);
                $('#empty-alert-error').removeClass('hidden');
            }
        }).done(function(data) {
            $('#contact-request').addClass('is-disabled');
            $('#contact-request > .btn__text').text('request sent');
            $('#contact-request-trigger').addClass('is-disabled');

            $('#empty-alert-error').hide();
            $('#empty-alert-success > .alert__body > .empty').text(data.message);
            $('#empty-alert-success').removeClass('hidden');

            /*setTimeout(function () {
                // window.location.replace(redirectTo);
                // window.location.reload();
            }, 2000);*/
        });
    })
});
